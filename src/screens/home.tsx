import React from 'react'
import { View, Text, Image, TouchableOpacity,ScrollView } from 'react-native'

interface HomeProps {
    navigation: any;
  }
const Home = (props:HomeProps) => {
    return (
        <ScrollView>
        <View>
                <View style={{backgroundColor:'#BC4303',height:480,justifyContent:'center',alignItems:'center' }}>
                    <View style={{
                        justifyContent:'center',
                        alignItems:'center', 
                        height:60,
                        width:350,
                        borderWidth:2,
                        borderColor:'#FFFFFF',
                        borderRadius:1,
                        marginBottom:3}}>

                        <Text style={{fontSize:32,fontWeight:'italic' }}>
                        AL QURANULKARIM</Text>
                    </View>
                    <View style={{justifyContent:'center',
                        alignItems:'center'}}>
                        <Image 
                        source={require('../gambar/bsm.png')}
                        style={{height:100,width:170}}
                        />
                    </View>
                    <View style={{justifyContent:'center',
                        alignItems:'center', 
                        height:270,
                        gold:270, 
                        borderWidth:3,
                        borderColor:'#FFFFFF',
                        borderRadius:150,
                        marginTop:0}}>
                        <Image 
                        source={require('../gambar/aa.png')}
                        style={{width:250,height:250}}
                        />
                    </View>            
                </View>
                <View style={{flexDirection:'row',backgroundColor:'#BC4303',height:400,flexWrap:'wrap',justifyContent:'center',alignItems:'center',}}>
                <View >
                        <TouchableOpacity
                                  onPress = {() => 
                                    props.navigation.navigate('Home')
                                }>

                    <View style={{
                            justifyContent:'center',
                            alignItems:'center', 
                            height:100, 
                            width:100,
                            borderWidth:2,
                            borderColor:'#FFFFFF',
                            borderRadius:30,
                            marginTop:250,                           
                            marginLeft:18}}>

                        <Image 
                        source={require('../gambar/qr.png')}
                        style={{width:60,height:60}}
                        />
                        <Text>AL QURAN</Text>
                    </View>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <TouchableOpacity
                       onPress = {() => 
                        props.navigation.navigate('As')
                    }>

                    <View style={{
                            justifyContent:'center',
                            alignItems:'center', 
                            height:100, 
                            width:100,
                            borderWidth:2,
                            borderColor:'#FFFFFF',
                            borderRadius:30,
                            marginTop:250,
                            marginLeft:10}}>

                    <Image 
                        source={require('../gambar/as.jpg')}
                        style={{width:60,height:60}}
                        />
                        <Text style={{fontSize:14}}>ASMAUL HUSNA</Text>
                    </View>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <TouchableOpacity
                        onPress = {() => 
                            props.navigation.navigate('Tahlil')
                        }>

                    <View style={{
                            justifyContent:'center',
                            alignItems:'center', 
                            height:100, 
                            width:100,
                            borderWidth:2,
                            borderColor:'#FFFFFF',
                            borderRadius:30,
                            marginTop:250,
                            marginLeft:10}}>

                    <Image 
                        source={require('../gambar/dzikir.png')}
                        style={{width:60,height:60}}
                        />
                        <Text>DZIKIR</Text>
                    </View>
                        </TouchableOpacity>
                    </View>

                    
                    
                    </View>

                    
                   
        </View>
        </ScrollView>
    )
}

export default Home




